package hk.com.novare.master.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;

public class ExcelUtil {
	
	private XSSFRow row;
	private Cell cell;
	
	protected String getCell(int cellno) {
		try {
			cell = row.getCell(cellno);
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_NUMERIC:
				return Double.toString(cell.getNumericCellValue());
			case Cell.CELL_TYPE_STRING:
				if (cell.getStringCellValue() != null) {
					return cell.getStringCellValue();
				}
				break;
			case Cell.CELL_TYPE_BLANK:
				break;
			}
			return "";
		} catch (Exception e) {
			return "";
		}
	}
}
