package hk.com.novare.master.xpaths;

public class SecurityRelatedXpath {
	
	/*
	 *	generic_field		add '[ index ]/td[2]/input' at the end
	 *						index starts at 2
	 * 	
	 * */
	public static final String GENERIC_FIELD = ".//*[@id='genericPopupContent']/div/div[2]/table/tbody/tr[";
	public static final String NEW_ENTRY = ".//*[@id='newEntryLinkId']";
	public static final String SAVE_DRAFT = ".//*[@id='buttonGroup']/input[3]";
	public static final String SUBMIT = ".//*[@id='buttonGroup']/input[4]";
	
	//ENTITY
	public static final String SECURITY_PARAMETER = ".//*[@id='securityrelated']/a[1]/div";
	public static final String SECURITY_QUESTION = ".//*[@id='securityrelated']/a[2]/div";
	public static final String PASSWORD_BLACKLIST = ".//*[@id='securityrelated']/a[3]/div";
	public static final String BUSINESS_CENTER = ".//*[@id='securityrelated']/a[4]/div";
	public static final String CELL_GROUP = ".//*[@id='securityrelated']/a[5]/div";
	public static final String BOOKING_COMPANY = ".//*[@id='securityrelated']/a[6]/div";
	public static final String BRANCH = ".//*[@id='securityrelated']/a[7]/div";
	public static final String GROUP_TYPE = ".//*[@id='securityrelated']/a[8]/div";
	public static final String MARKERTING_AREA = ".//*[@id='securityrelated']/a[9]/div";
	public static final String DEPARTMENT_NAME = ".//*[@id='securityrelated']/a[10]/div";
	public static final String CURRENCY = ".//*[@id='securityrelated']/a[11]/div";
	public static final String USER = ".//*[@id='securityrelated']/a[12]/div";
	public static final String POSITION = ".//*[@id='securityrelated']/a[13]/div";
	
	//DATE
	public static final String PB_VALID_FROM = ".//*[@id='dp1402642276111']";
	public static final String PB_VALID_UNTIL = ".//*[@id='dp1402642276112']";
	public static final String BC_VALID_FROM = ".//*[@id='dp1402642377539']";
	public static final String BC_VALID_UNTIL = ".//*[@id='dp1402642377540']";
	public static final String MA_VALID_FROM = ".//*[@id='dp1402642461710']";
	public static final String MA_VALID_UNTIL = ".//*[@id='dp1402642461711']";
	public static final String DN_VALID_FROM = ".//*[@id='dp1402642505553']";
	public static final String DN_VALID_UNTIL = ".//*[@id='dp1402642505554']";
	public static final String C_VALID_FROM = ".//*[@id='dp1402642556201']";
	public static final String C_VALID_UNTIL = ".//*[@id='dp1402642556202']";
	public static final String U_VALID_FROM = ".//*[@id='dp1402642622770']";
	public static final String U_VALID_UNTIL = ".//*[@id='dp1402642622771']";
	
}
