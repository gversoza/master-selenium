package hk.com.novare.master.triggers;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestTrigger  {
	
	public TestTrigger trigger;
	public InternetExplorerDriver driver, driver2;
	public WebElement element;
	
	public TestTrigger(String url) {
		System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.navigate().to(url + "/eloans/app/login");
		// SIT - http://10.23.22.68:9081/eloans-admin/app/login
	}

	public void inputByXpath(String xpathExpression, String input) {
		try {
			this.waits(10);
			element = driver.findElement(By.xpath(xpathExpression));
			element.sendKeys(input);
		} catch (NoSuchElementException e) {
			System.out.println("NoSuchElementException.. Retrying..");
			this.sleep(5000);
			this.inputByXpath(xpathExpression, input);
		} catch (InvalidElementStateException e) {
			System.out.println("InvalidElementStateException.. Retrying..");
			this.sleep(5000);
			this.inputByXpath(xpathExpression, input);
		} catch (ElementNotVisibleException e) {
			this.sleep(5000);
			this.inputByXpath(xpathExpression, input);
		}
	}

	public void selectInputByXpath(String xpathExpression, String input) {
		try {
			Select select = new Select(driver.findElement(By
					.xpath(xpathExpression)));
			select.selectByVisibleText(input);
		} catch (NoSuchElementException e) {
			System.out.println("NoSuchElementException.. Retrying..");
			this.sleep(5000);
			this.selectInputByXpath(xpathExpression, input);
		} catch (InvalidElementStateException e) {
			System.out.println("InvalidElementStateException.. Retrying..");
			this.sleep(5000);
			this.selectInputByXpath(xpathExpression, input);
		}
	}

	public void selectInputByIndex(String xpath, int index) {
		try {
			Select select = new Select(driver.findElement(By.xpath(xpath)));
			select.selectByIndex(index);
		} catch (NoSuchElementException e) {
			System.out.println("NoSuchElementException.. Retrying..");
			this.waits(10);
			this.selectInputByIndex(xpath, index);
		} catch (IndexOutOfBoundsException e) {
			System.out.println("IndexOutOfBoundsException.. Retrying..");
			this.selectInputByIndex(xpath, index - 1);
		}
	}

	public boolean isElementEnabled(String xpath) {

		try {
			return driver.findElement(By.xpath(xpath)).isEnabled();
		} catch (NoSuchElementException e) {
			return false;
		} catch (ElementNotVisibleException e) {
			return false;
		}
	}

	public void selectByVisibleText(String xpath, String value) {
		try {
			Select select = new Select(driver.findElementByXPath(xpath));
			if (select.getAllSelectedOptions().size() > 1)
				select.selectByIndex(1);
			Thread.sleep(5000);
			select.selectByVisibleText(value);
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void clickByXpath(String xpathExpression) {
		try {
			element = driver.findElement(By.xpath(xpathExpression));
			element.submit();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}

	public void keyDownOn(String xpath) {
		try {
			element = driver.findElement(By.xpath(xpath));
			element.sendKeys(Keys.DOWN);
		} catch (InvalidElementStateException e) {
			this.keyDownOn(xpath);
		} catch (NoSuchElementException e) {
			this.keyDownOn(xpath);
		}
	}

	public void doubleKeyDownOn(String xpath) {
		try {
			element = driver.findElement(By.xpath(xpath));
			element.sendKeys(Keys.DOWN);
			element.sendKeys(Keys.DOWN);
		} catch (InvalidElementStateException e) {
			this.keyDownOn(xpath);
		} catch (NoSuchElementException e) {
			this.keyDownOn(xpath);
		}
	}

	public void keyUpOn(String xpath) {
		try {
			element = driver.findElement(By.xpath(xpath));
			element.sendKeys(Keys.UP);
		} catch (InvalidElementStateException e) {
			this.keyUpOn(xpath);
		} catch (NoSuchElementException e) {
			this.keyUpOn(xpath);
		}
	}

	public void clickElement(String xpath) {
		try {
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			element = driver.findElementByXPath(xpath);
			element.click();
		} catch (NoSuchElementException e) {
			System.out
			.println("clickElement() NoSuchElementException.. Retrying..");
			this.sleep(5000);
			this.clickElement(xpath);
		} catch (InvalidElementStateException e) {
			System.out
			.println("clickElement() InvalidElementStateException.. Retrying..");
			this.clickElement(xpath);
		} catch (ElementNotVisibleException e) {
			System.out
			.println("clickElement() ElementNotVisibleException.. Retrying..");
			this.sleep(5000);
			this.clickElement(xpath);
				
		} catch (StaleElementReferenceException e) {
			System.out
			.println("clickElement() StaleElementReferenceException.. Retrying..");
			this.sleep(5000);
			this.clickElement(xpath);
		} catch (WebDriverException e) {
			System.out
			.println("clickElement() WebDriverException");
			this.sleep(5000);
		}

	}

	public void selectThenEnterElement(String xpath) {
		try {

			element = driver.findElementByXPath(xpath);
			element.sendKeys(Keys.ENTER);
		} catch (NoSuchElementException e) {
			this.clickElement(xpath);
			System.out
					.println("clickElement() NoSuchElementException.. Retrying..");
		} catch (InvalidElementStateException e) {
			this.clickElement(xpath);
			System.out
					.println("clickElement() InvalidElementStateException.. Retrying..");
		} catch (ElementNotVisibleException e) {
			this.clickElement(xpath);
			System.out
					.println("clickElement() ElementNotVisibleException.. Retrying..");

		} catch (StaleElementReferenceException e) {
			this.clickElement(xpath);
			System.out
					.println("clickElement() StaleElementReferenceException.. Retrying..");
		} catch (WebDriverException e) {
			this.clickElement(xpath);
			System.out
					.println("clickElement() WebDriverException.. Retrying..");
		}

	}

	public void clickElementByEnterKey(String xpath) {
		try {
			this.waits(10);
			element = driver.findElementByXPath(xpath);
			this.enterKey();
		} catch (NoSuchElementException e) {
			this.clickElementByEnterKey(xpath);
		} catch (InvalidElementStateException e) {
			this.clickElementByEnterKey(xpath);
		} catch (StaleElementReferenceException e) {
			this.clickElementByEnterKey(xpath);
		}
	}

	public void waitForPageToLoad(int timeout) {
		driver.manage().timeouts().pageLoadTimeout(timeout, TimeUnit.SECONDS);
	}

	public String getIntegersFromString(String s) {
		String newString = "";
		for (int i = 0; i < s.length(); i++) {
			if (Character.isDigit(s.charAt(i))) {
				newString += s.charAt(i);
			}
		}
		return newString;
	}

	public void waitForElementPresent(final By by, int timeout) {
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(driver, timeout)
				.ignoring(StaleElementReferenceException.class);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver webDriver) {
				WebElement element = webDriver.findElement(by);
				return element != null && element.isDisplayed();
			}
		});
	}

	public WebElement waitForElement(By by) {
	    WebElement result = null;
	    long maxTime = 5 * 1000; // time in milliseconds
	    long timeSlice = 250;
	    long elapsedTime = 0;

	    do {
	        try{
	            Thread.sleep(timeSlice);
	            elapsedTime += timeSlice;
	            result = driver.findElement(by);
	        } catch(Exception e) {
	        }
	    } while(result == null && elapsedTime < maxTime);

	    return result;
	}
	
	public void waitForElementPresent(final By by) {
		waitForElementPresent(by, 50);
	}

	public void waitForPageLoad() {

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 50);
		try {
			wait.until(expectation);
		} catch (Throwable error) {
			throw new org.openqa.selenium.TimeoutException(
					"Timeout waiting for Page Load Request to complete.");
		}
	}

	public String getSelectedText(String xpathExpression) {
		String value = "";
		try {
			element = driver.findElement(By.xpath(xpathExpression));
			Select selectedValue = new Select(element);
			value = selectedValue.getFirstSelectedOption().getText();

		} catch (NoSuchElementException e) {
			this.getSelectedText(xpathExpression);
		}
		return value;
	}

	public String getText(String xpathExpression) {
		try {
			element = driver.findElement(By.xpath(xpathExpression));

		} catch (NoSuchElementException e) {
			return "";
		} catch (NullPointerException e) {
			return "";
		}
		return element.getText();
	}

	public boolean checkIfElementIsEnabled(String xpathExpression) {
		boolean state;
		try {
			element = driver.findElement(By.xpath(xpathExpression));
			state = element.isEnabled();
		} catch (NoSuchElementException e) {
			state = false;
			e.printStackTrace();
		}
		return state;
	}
	

	public boolean isElementExisting(String xpath) {
		try {
			driver.findElementByXPath(xpath);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	

	//IMPLICITLY WAITS
	public void waits(long secs) {
		try{
			driver.manage().timeouts().implicitlyWait(secs, TimeUnit.SECONDS);
		}catch(SessionNotFoundException e){}
	}

	
	public void sleep(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean checkIfElementIsDisplayed(String xpathExpression) {
		boolean state;
		try {
			element = driver.findElement(By.xpath(xpathExpression));
			state = element.isDisplayed();
		} catch (NoSuchElementException e) {
			state = false;
			this.checkIfElementIsDisplayed(xpathExpression);
		} catch (StaleElementReferenceException e){
			state = false;
			this.checkIfElementIsDisplayed(xpathExpression);
		} catch (WebDriverException e){
			state = false;
			this.checkIfElementIsDisplayed(xpathExpression);
		}
		return state;
	}

	public void clearInputByXpath(String xpathExpression) {
		try {
			driver.findElement(By.xpath(xpathExpression)).clear();
		} catch (NoSuchElementException e) {
			System.out.println("NoSuchElementException.. Retrying..");
			this.sleep(5000);
			this.clearInputByXpath(xpathExpression);	
		} catch (InvalidElementStateException e) {
			System.out.println("InvalidElementStateException.. Retrying..");
			this.sleep(5000);
			this.clearInputByXpath(xpathExpression);
		}
	}

	public boolean isTicked(String xpath) {
		try {
			boolean isChecked = driver.findElement(By.xpath(xpath))
					.isSelected();
			return isChecked;
		} catch (NoSuchElementException e) {
			return false;
		} catch (ElementNotVisibleException e) {
			return false;
		}
	}

	public void tick(String xpath) {
		try {
			element = driver.findElement(By.xpath(xpath));
			element.sendKeys(Keys.SPACE);
		} catch (NoSuchElementException e) {
			this.sleep(5000);
			this.tick(xpath);
		} catch (ElementNotVisibleException e) {
			this.sleep(5000);
			this.tick(xpath);
		}
	}

	public void acceptAlert() {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (NoAlertPresentException e) {
			this.acceptAlert();
		}
	}

	public void rejectAlert() {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	public boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) {
			return false;
		}
	}

	public String getAlertMessage() {
		return driver.switchTo().alert().getText();
	}

	public void enterKey() {
		driver.getKeyboard().sendKeys(Keys.ENTER);
	}

	public void spaceKey() {
		driver.getKeyboard().sendKeys(Keys.SPACE);
	}

	public void tabKey() {
		driver.getKeyboard().sendKeys(Keys.TAB);
	}

	public void pageDownKey() {
		driver.getKeyboard().sendKeys(Keys.PAGE_DOWN);
	}

	public void pageUpKey() {
		driver.getKeyboard().sendKeys(Keys.PAGE_UP);
	}

	public void refreshKey() {
		driver.getKeyboard().sendKeys(Keys.F5);
	}

	public void shiftTab() {
		driver.getKeyboard().pressKey(Keys.SHIFT);
		driver.getKeyboard().sendKeys(Keys.TAB);
		driver.getKeyboard().releaseKey(Keys.SHIFT);
	}

	public void downKey() {
		driver.getKeyboard().sendKeys(Keys.DOWN);
	}

	public void close() {
		driver.close();
	}

	public void quit() {

		driver.quit();
	}
	
	public void newWindow() {

		driver.quit();
		driver = new InternetExplorerDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.navigate().to("http://10.23.15.145:9085/eloans/app/login");
	}

}